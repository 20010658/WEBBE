<?php
function queryConferenze(){
  $db=mysqli_connect('127.0.0.1','root','') or die('Connessione al server fallita  ' . mysqli_error());
  mysqli_select_db($db, 'conference') or die('Impossibile accedere al db conference '. mysqli_error());

  $query="SELECT * FROM conferenza";

  $result=mysqli_query($db,$query);
  if(mysqli_num_rows($result) == 0){
    return("<h1>Nessuna conferenza disponibile nel Database! :(</h1>");
  }else{
    return($result);
  }
}
 ?>

<div class="container">
    <div class="row text-center">
      <span class="page_info"><h3>Seleziona la conferenza per cui ti vuoi prenotare!</h3></span>
      <div id="container_conferenze">
        <?php
          $result = queryConferenze();
          $onClick = "";
          while($row = mysqli_fetch_row($result)){
            print('
              <div class="conferenza" onClick={handleConferenzaClick('.$row[0].');}>
                <ul class="conferenza_list">
                  <li>Titolo: '.$row[1].'</li>
                  <li>Luogo: '.$row[2].'</li>
                  <li>Data di inizio: '.$row[3].'</li>
                  <li>Data di fine: '.$row[4].'</li>
                </ul>
              </div>
            ');
          }
         ?>
      </div>
    </div>
<script type="text/javascript">
  function handleConferenzaClick(idConferenza){
    window.location.href="home.php?source=1&conferenza="+idConferenza;
  }
</script>
</div>
