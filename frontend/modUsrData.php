<?php
  if(isset($_GET['attr']) && isset($_GET['val'])){
    $db=mysqli_connect('127.0.0.1','root','') or die('Connessione al server fallita  ' . mysqli_error());
    mysqli_select_db($db, 'conference') or die('Impossibile accedere al db conference '. mysqli_error());

    $attr = stripslashes($_GET['attr']);
    $attr = mysqli_real_escape_string($db,$attr);
    $val = stripslashes($_GET['val']);
    $val = mysqli_real_escape_string($db,$val);
    $action = "home.php?source=5&attr=".$attr."&val=".$val;
  }
 ?>
<div class="container">
  <div class="card w-50">
    <div class="card-body">
      <form action=<?php echo($action) ?> method="POST">
        <span class="page_info">
          Stai modificando il valore di:<strong> <?php if($val!=='p'){echo($attr.'('.$val.')');}else{echo($attr);} ?></strong>
        </span>
        <div class="form-group">
          <input type="text" class="form-control" id="newVal" name="newVal">
          <div id="mod_usr_data_buttons" style="padding-left: 30%;">
            <button type="submit" class="btn btn-primary" style="margin-top: 10px;">Procedi</button>
            <a href="home.php?source=3" class="btn btn-danger" style="margin-top: 10px;">Torna indietro</a>
          </div>
        </div>


  <?php
    if(isset($_POST['newVal'])){
      $newVal = stripslashes($_POST['newVal']);
      $newVal = mysqli_real_escape_string($db,$newVal);
      if($_SESSION['isAutore']==false){
        $query='UPDATE persona SET '.$attr.' = "'.$newVal.'" WHERE IDpersona = '.$_SESSION['userID'];
      }else{
        $query='UPDATE autore SET '.$attr.' = "'.$newVal.'" WHERE IDautore = '.$_SESSION['userID'];
      }
      $result = mysqli_query($db,$query);
      if(!$result){
        print('
          <div class="alert alert-danger" role="alert">
            <strong>Errore!</strong> Ci sono problemi di sistema, ti preghiamo di riprovare più tardi :('.$query.'
          </div>
          ');
      }else{
        if($attr == "nome"){
          $_SESSION['userNOME'] = $newVal;
        }else if($attr == "cognome"){
          $_SESSION['userCOGNOME'] = $newVal;
        }
        print('
          <div class="alert alert-success" role="alert">
            <strong>Successo!</strong> La modifica è andata a buon fine.
          </div>
          ');
      }
    }
  ?>
</div>
</div>
</div>
