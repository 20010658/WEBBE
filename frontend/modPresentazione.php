<?php
$action="home.php?source=8&event=".$_GET['event'];
$db=mysqli_connect('127.0.0.1','root','') or die('Connessione al server fallita  ' . mysqli_error($db));
mysqli_select_db($db, 'conference') or die('Impossibile accedere al db conference '. mysqli_error($db));
$evento = stripslashes($_GET['event']);
$evento = mysqli_real_escape_string($db,$evento);

$query = 'SELECT path FROM presentazione';

$result=mysqli_query($db,$query) or die('Query non eseguibile  '. mysqli_error($db));
$oldPath="";
while($row = mysqli_fetch_row($result)){
  if($row[0]!=null){
    $oldPath = $row[0];
  }
}
 ?>
<div class="container">
  <div class="card w-50">
    <div class="card-body">
        <span class="page_info">
          Puoi cambiare il file oppure cancellarlo, attuale -> <strong> <?php echo basename($oldPath) ?></strong>
        </span>
        <form action=<?php echo('"'.$action.'"');?> method="POST" enctype="multipart/form-data">
          <div class="form-group">
           <input id="pres" name="presentazione" type="file" class="file">
           <div id="mod_usr_data_buttons" style="padding-left: 30%;">
              <button type="submit" class="btn btn-primary" name="mod" style="margin-top: 10px;">Modifica</button>
              <button type="submit" class="btn btn-danger" name="del" style="margin-top: 10px;">Cancella</button>
              <a href="home.php?source=4" class="btn btn-secondary" style="margin-top: 10px;">Torna indietro</a>
           </div>
          </div>
        </form>

<?php
  $target_dir = "uploads/".$_SESSION['userID'].'/';
  if(file_exists($target_dir)){
    if(isset($_POST['mod'])){
      //voglio modificare il file
      if($_FILES["presentazione"]["name"]!=""){
          $target_file = $target_dir . basename($_FILES["presentazione"]["name"]);
        if(deleteFile($oldPath)){
          if (move_uploaded_file($_FILES["presentazione"]["tmp_name"], $target_file)) {
            if(updateDB($target_file, basename( $_FILES["presentazione"]["name"]))!="error"){
              print('
                <div class="alert alert-success" role="alert">
                  <strong>Successo!</strong> Il file '.basename( $_FILES["presentazione"]["name"]).' è stato caricato.
                </div>
                ');
            }else{
              print('
                <div class="alert alert-danger" role="alert">
                  <strong>Errore!</strong> Non è stato possibile aggiornare il database, il file sarà eliminato dal server
                </div>
                ');
            }
          }else{
              print('
                <div class="alert alert-danger" role="alert">
                  <strong>Errore!</strong> Info -> '.$_FILES['userfile']['error'].'
                </div>
                ');
          }
        }else{
          print('
            <div class="alert alert-danger" role="alert">
              <strong>Errore!</strong> Impossibile eliminare il file '.basename( $oldPath).'.
            </div>
            ');
        }
      }else{
        print('
          <div class="alert alert-danger" role="alert">
            <strong>Errore!</strong> Per modificare, devi aver selezionato il nuovo file.
          </div>
          ');
      }
    }else if(isset($_POST['del'])){
      //voglio cancellare
      if(deleteFile($oldPath)){
        print('
          <div class="alert alert-success" role="alert">
            <strong>Successo!</strong> Il file '.basename( $oldPath).' è stato eliminato.
          </div>
          ');
      }else{
        print('
          <div class="alert alert-danger" role="alert">
            <strong>Errore!</strong> Impossibile eliminare il file '.basename( $oldPath).'
          </div>
          ');
      }
    }
  }

function deleteFile($path){
  $res=true;
  if(file_exists($path)){
    chmod($path,0777);
    $res = unlink(''.$path);
    if($res==false){
       return false;
     }else{
      updateDB(null,null);
      return true;
     }
  }else return false;
}
function updateDB($path, $filename){
	$db=mysqli_connect('127.0.0.1','root','') or die('Connessione al server fallita  ' . mysqli_error($db));
	mysqli_select_db($db, 'conference') or die('Impossibile accedere al db conference '. mysqli_error($db));
	if($filename!=null){
		$filename = stripslashes($filename);
		$filename = mysqli_real_escape_string($db,$filename);
	}
	if($path!=null){
		$path = stripslashes($path);
		$path = mysqli_real_escape_string($db,$path);
	}
	
	if($path==null && $filename==null){
		$queryUpdate = 'DELETE FROM `presentazione` WHERE IDautore="'.$_SESSION['userID'].'" AND IDevento="'.$evento.'"';
		$result=mysqli_query($db,$queryUpdate) or die('Query non eseguibile  '. mysqli_error($db));
	}
	else{
		$evento = stripslashes($_GET['event']);
		$evento = mysqli_real_escape_string($db,$evento);
		$queryUpdate = 'DELETE FROM `presentazione` WHERE IDautore="'.$_SESSION['userID'].'" AND IDevento="'.$evento.'"';

		$result=mysqli_query($db,$queryUpdate) or die('Query non eseguibile  '. mysqli_error($db));
  
		$queryUpdate = 'INSERT INTO `presentazione`( `filename`, `path`,`IDevento`,`IDautore`) VALUES ("'.$filename.'","'.$path.'","'.$evento.'","'.$_SESSION['userID'].'")';
		$result=mysqli_query($db,$queryUpdate) or die('Query non eseguibile  '. mysqli_error($db));
	}

	if(!$result){
		return('error');
	}else{
		return('ok');
	}
}
?>
</div>
</div>
</div>
