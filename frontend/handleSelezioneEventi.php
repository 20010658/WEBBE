<?php
  if(isset($_POST['eventi'])){
    $db=mysqli_connect('127.0.0.1','root','') or die('Connessione al server fallita  ' . mysqli_error());
    mysqli_select_db($db, 'conference') or die('Impossibile accedere al db conference '. mysqli_error());
    $query='SELECT * FROM evento e,partecipa p,persona pe WHERE e.IDevento=p.IDevento AND p.IDpersona='.$_SESSION['userID'].' AND p.IDpersona=pe.IDpersona AND e.IDconferenza='.$_SESSION['conferenza'];
    $foundScDB=false;
    $result=mysqli_query($db,$query);
    if(mysqli_num_rows($result)!=0){
      //se tra gli eventi della conferenza già prenotati c'è uno scientifico, non controllo neanche cosa ha selezionato l'utente
      while($row = mysqli_fetch_row($result)){
        if($row[8]=='t'){
           $foundScDB=true;
           break;
        }
      }
    }else{
      $foundScDB=false;
    }
    if($foundScDB){
      //carico la selezione sul DB, visto che l'utente ha già prenotato eventi scientifici
      $res = loadSelection();
      if($res==false){
        carica_costo();
        print('<span class="page_info">La prenotazione è stata salvata correttamente nel sistema!<br><a href="home.php?source=0">Clicca qui per tornare alla home</a></span>');
      }else{
        print('<span class="page_info">Attenzione, si è verificato un problema nel sistema!<br><a href="home.php?source=0">Clicca qui per tornare alla home</a></span>');
      }
    }else{
      //controllo la selezione dell'utente, se ok carico su DB
      if(checkIfOnlySvago()){
        $res = loadSelection();
        if($res==false){
          carica_costo();
          print('<span class="page_info">La prenotazione è stata salvata correttamente nel sistema!<br><a href="home.php?source=0">Clicca qui per tornare alla home</a></span>');
        }else{
          print('<span class="page_info">Attenzione, si è verificato un problema nel caricamento della prenotazione per '.$res.'<br><a href="home.php?source=0">Clicca qui per tornare alla home</a></span>');
        }
      }else{
        print('<span class="page_info">Errore! devi prima aver prenotato o selezionato almeno un evento scientifico di questa conferenza!<br><a href="home.php?source=1&conferenza='.$_SESSION['conferenza'].'">Clicca qui per tornare indietro</a></span>');
      }
    }
  }
  function carica_costo(){
    $db=mysqli_connect('127.0.0.1','root','') or die('Connessione al server fallita  ' . mysqli_error());
    mysqli_select_db($db, 'conference') or die('Impossibile accedere al db conference '. mysqli_error());
    $query='SELECT SUM(costo) FROM `partecipa`,evento WHERE IDpersona='.$_SESSION['userID'].' AND partecipa.IDevento=evento.IDevento';
    $result=mysqli_query($db,$query);
    if(mysqli_num_rows($result)==0){
      return false;
    }else{
      $row=mysqli_fetch_row($result);

      $query='UPDATE persona SET costo='.$row[0].' WHERE IDpersona='.$_SESSION['userID'];
      $result=mysqli_query($db,$query);
      if(!$result){
        return false;
      }else return true;
    }
    }
  function loadSelection(){
    $a = $_POST['eventi'];
    $db=mysqli_connect('127.0.0.1','root','') or die('Connessione al server fallita  ' . mysqli_error());
    mysqli_select_db($db, 'conference') or die('Impossibile accedere al db conference '. mysqli_error());

    foreach ($a as $IDevento) {
      $query = 'INSERT INTO partecipa (`IDpersona`, `IDevento`, `commento`) VALUES ('.$_SESSION['userID'].','.$IDevento.', NULL)';
      $result=mysqli_query($db,$query);
      $err=false;
      if(!$result){
        $err = $IDevento;
        break;
      }
    }
    return $err;
  }
  //controlla ciò che l'usr ha selezionato, true se nella selezione dell'utente c'è almeno un evento scientifico
  function checkIfOnlySvago(){
    $a = $_POST['eventi'];
    $db=mysqli_connect('127.0.0.1','root','') or die('Connessione al server fallita  ' . mysqli_error());
    mysqli_select_db($db, 'conference') or die('Impossibile accedere al db conference '. mysqli_error());
    //prelevo dal db le info sugli eventi che l'usr ha selezionato
    $query='SELECT * FROM evento WHERE IDconferenza='.$_SESSION['conferenza'].' AND IDevento IN  (' . implode(',', array_map('intval', $a)) . ')';
    $result=mysqli_query($db,$query);
    while($row = mysqli_fetch_row($result)){
        if($row[8]=='t'){
          return true;
        }
    }
    return false;
    }
 ?>
