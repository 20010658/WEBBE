<?php
	function queryEventi(){
		
		$db=mysqli_connect('127.0.0.1','root','') or die('Connessione fallita al server'. mysqli_error($db));
		mysqli_select_db($db,'conference') or die ('Connessione a conference fallita'. mysqli_error($db));
		
		$conferenza=stripcslashes($_GET['conferenza']);
		$conferenza=mysqli_real_escape_string($db, $conferenza);
		
		$_SESSION['conferenza']=$conferenza;
		$queryEventiConferenza="SELECT * FROM evento WHERE IDconferenza=".$conferenza;
		$resEvConf=mysqli_query($db,$queryEventiConferenza) or die('Query non eseguibile  '. mysqli_error());
		
		if(mysqli_num_rows($resEvConf)==0){
			return('error');
		}
		else{
			return($resEvConf);
		}
	}
	
	function queryEventiGiàPrenotati(){
		$ID=
		$db=mysqli_connect('127.0.0.1','root','') or die('Connessione fallita al server'. mysqli_error($db));
		mysqli_select_db($db,'conference') or die ('Connessione a conference fallita'. mysqli_error($db));
	
		$queryEventiGiaPrenotati = "SELECT IDevento FROM partecipa WHERE IDpersona='".$_SESSION['userID']."'";
		
		$resEvPrenot = mysqli_query($db, $queryEventiGiaPrenotati) or die ('Query non eseguibile'. mysqli_error($db));
		
		return $resEvPrenot;
	}
	
	function queryDataStop(){
		
		$db=mysqli_connect('127.0.0.1','root','') or die('Connessione fallita al server'. mysqli_error($db));
		mysqli_select_db($db,'conference') or die ('Connessione a conference fallita'. mysqli_error($db));
		
		$query = "SELECT data_stop_prenotazioni FROM conferenza WHERE IDconferenza=".$_SESSION['conferenza'];

		$res = mysqli_query($db,$query);
		if($res!=true || mysqli_num_rows($res)!=0){
			$now = Date('Y-m-d');
			if($res!=true||$now<mysqli_fetch_row($res)[0])
				return false;
			else return true;
		}else return true;
	}
	?>
	<div class="container">
    <div class="row text-center">
      <span class="page_info">
        <h3>Ecco gli eventi disponibili per questa conferenza</h3>
        <small class="form-text text-muted">Attenzione, puoi prenotare un evento di svago soltanto se hai prenotato almeno un evento scientifico.</small>
        <small class="form-text text-muted">[arancione=scientifico, verde=svago]</small>
      </span>

      <script type="text/javascript">
        function mostraTipologiaSelezionata(){
          if($('#sel1').val()!=='Tutti'){
            for(let i=0;i<$("#container_eventi div").length;i++){
              if($('#'+i).text().indexOf($('#sel1').val())===-1){
                $('#'+i).hide();
              }else{
                $('#'+i).show();
              }
            }
          }else{ //se l'utente ha selezionato Tutti, ciclo su ognuno e lo mostro
          for(let i=0;i<$("#container_eventi div").length;i++){
            $('#'+i).show();
          }
        }
      }
      </script>
      <div id="container_eventi">
        <form action="home.php?source=2" method="POST">
          <div id="eventi_tools">
            <button type="submit" class="btn btn-primary" id="prenota_button">Prenota</button>
            <select class="form-control text-center" id="sel1" onchange={mostraTipologiaSelezionata()}>
              
              <option value="Tutti" selected>Tutti</option>
          </select>
        </div>

          <?php
            $eventiGiaPrenotati = array();
            $i=0;
            $result = queryEventi();
            $prenotati = queryEventiGiàPrenotati();
            while($row = mysqli_fetch_row($prenotati)){
              $eventiGiaPrenotati[$i] = $row[0];
              $i++;
            }
            $stopPrenotazioni = queryDataStop();
            if($result=='error'){print("<h1>Nessun evento disponibile nel Database! :(</h1>");}
            else{
              $nEventi=0;
              while($row = mysqli_fetch_row($result)){
                if($row[8]=='t') $scientifico="_scientifico";
                else $scientifico="_svago";
                if(!in_array($row[0],$eventiGiaPrenotati)){
                  if($stopPrenotazioni){
                    print('
                      <div class="evento'.$scientifico.'" id="'.$nEventi.'">
                        <ul class="evento_list">
                          <li>Titolo: '.$row[1].'</li>
                          <li>Tipologia: '.$row[2].'</li>
                          <li>Descrizione: '.$row[3].'</li>
                          <li>Luogo: '.$row[4].'</li>
                          <li>Ora: '.$row[5].'</li>
                          <li>Giorno: '.$row[7].'</li>
                          <li>Costo: '.$row[6].'€</li>
                        </ul>
                        Tempo scaduto per prenotare!
                      </div>
                    ');
                  }else{
                    print('
                      <div class="evento'.$scientifico.'" id="'.$nEventi.'">
                        <ul class="evento_list">
                          <li>Titolo: '.$row[1].'</li>
                          <li>Tipologia: '.$row[2].'</li>
                          <li>Descrizione: '.$row[3].'</li>
                          <li>Luogo: '.$row[4].'</li>
                          <li>Ora: '.$row[5].'</li>
                          <li>Giorno: '.$row[7].'</li>
                          <li>Costo: '.$row[6].'€</li>
                        </ul>
                        <input type="checkbox" name="eventi[]" value="'.$row[0].'">
                      </div>
                    ');
                  }
                }else{
                  print('
                    <div class="evento'.$scientifico.'" id="'.$nEventi.'">
                      <ul class="evento_list">
                        <li>Titolo: '.$row[1].'</li>
                        <li>Tipologia: '.$row[2].'</li>
                        <li>Descrizione: '.$row[3].'</li>
                        <li>Luogo: '.$row[4].'</li>
                        <li>Ora: '.$row[5].'</li>
                        <li>Giorno: '.$row[7].'</li>
                        <li>Costo: '.$row[6].'€</li>
                      </ul>
                      Hai già prenotato il posto per questo evento
                    </div>
                  ');
                }
                $nEventi++;
              }
            }
          ?>

        </form>
      </div>

    </div>
  </div>