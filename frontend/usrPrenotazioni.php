<div class="container">
  <div class="row text-center">
    <span class="page_info">
		<h3>Ecco gli eventi per cui hai effetuato la prenotazione!</h3>
    </span>
	<?php
	$db=mysqli_connect('127.0.0.1','root','') or die('Connessione al server fallita  ' . mysqli_error($db));
	mysqli_select_db($db, 'conference') or die('Impossibile accedere al db conference '. mysqli_error($db));
	//                evento--------------conferenza---------------------
	if($_SESSION['isAutore'] ==false){
		$query='SELECT e.titolo,e.luogo,e.ora,e.giorno,c.titolo,c.data_inizio,c.data_fine,p.IDpersona,p.IDevento FROM evento e,partecipa p, conferenza c WHERE e.IDevento=p.IDevento AND p.IDpersona='.$_SESSION['userID'].'  AND e.IDconferenza=c.IDconferenza ORDER BY c.IDconferenza';
	}
	else{
		$query='SELECT e.titolo,e.luogo,e.ora,e.giorno,c.titolo,c.data_inizio,c.data_fine,p.IDautore,p.IDevento FROM evento e,partecipa_autore p, conferenza c WHERE e.IDevento=p.IDevento AND p.IDautore='.$_SESSION['userID'].'  AND e.IDconferenza=c.IDconferenza ORDER BY c.IDconferenza';
	}
	$result = mysqli_query($db,$query) or die('Query non eseguibile  '. mysqli_error($db));
	if(mysqli_num_rows($result) == 0){
		return('<span class="page_info"><h1>Errore, non riusciamo a trovare le tue prenotazioni nei nostri database, prova più tardi :(</h1></span>');
	}else{
		if($_SESSION['isAutore']==true){
	print('
		<form action="home.php?source=4" method="POST">
		<table class="table">
		<thead>
			<tr>
				<th>Titolo</th>
				<th>Luogo</th>
				<th>Ora</th>
				<th>Giorno</th>
				<th>Nome Conferenza</th>
				<th>Data di inizio</th>
				<th>Data di fine</th>
				<th>Download File</th>
				<th>Azione</th>
			</tr>
		</thead>
		<tbody>

	');
	while($row = mysqli_fetch_row($result)){
		$query="SELECT path FROM presentazione WHERE IDevento=".$row[8];
		$presentazione = mysqli_query($db,$query) or die('Query non eseguibile  '. mysqli_error($db));
		if(mysqli_num_rows($presentazione) == 0){
       
		print('
			<tr>
			  <td>"'.$row[1].'"</td>
			  <td>"'.$row[2].'"</td>
			  <td>"'.$row[3].'"</td>
			  <td>"'.$row[4].'"</td>
			  <td>"'.$row[5].'"</td>
			  <td>"'.$row[6].'"</td>
			  <td>"'.$row[7].'"</td>
			  <td>Non disponibile</td>
			  <td><a href="home.php?source=9&IDp='.$row[7].'&IDe='.$row[8].'" class="btn btn-danger">Elimina</a>
			  <a href="home.php?source=10&IDp='.$row[7].'&IDe='.$row[8].'" class="btn btn-primary">Commenta</a>
			  <a href="home.php?source=7&event='.$row[8].'" class="btn btn-primary">Carica</a></td>
			</tr>
		');
    }else{
		print('
        <tr>
          <td>"'.$row[1].'"</td>
          <td>"'.$row[2].'"</td>
          <td>"'.$row[3].'"</td>
          <td>"'.$row[4].'"</td>
          <td>"'.$row[5].'"</td>
          <td>"'.$row[6].'"</td>
          <td>"'.$row[7].'"</td>
		  <td><a href="home.php?source=11&file='.mysqli_fetch_row($presentazione)[0].'" class="btn btn-success">Scarica</a></td>
          <td><a href="home.php?source=9&IDp='.$row[7].'&IDe='.$row[8].'" class="btn btn-danger">Elimina</a>
          <a href="home.php?source=10&IDp='.$row[7].'&IDe='.$row[8].'" class="btn btn-primary">Commenta</a>
		  <td><a href="home.php?source=8&event='.$row[0].'" class="btn btn-success">Modifica</a></td>
		</tr>
        ');
	}
  }
  /*$costo = queryCostoUsr();
  if($costo!=false){
    print('<p>Con queste prenotazioni devi pagare: '.$costo.'€</p>');
	}*/
	}
	else{
		 print('
  <form action="home.php?source=4" method="POST">
    <table class="table">
    <thead>
      <tr>
        <th>Titolo</th>
        <th>Luogo</th>
        <th>Ora</th>
        <th>Giorno</th>
        <th>Nome Conferenza</th>
        <th>Data di inizio</th>
        <th>Data di fine</th>
        <th>Download File</th>
        <th>Azione</th>
      </tr>
    </thead>
    <tbody>

  ');
  while($row = mysqli_fetch_row($result)){
    $query="SELECT path FROM presentazione WHERE IDevento=".$row[8];
    $presentazione = mysqli_query($db,$query) or die('Query non eseguibile  '. mysqli_error($db));
    if(mysqli_num_rows($presentazione) == 0){
    print('
      <tr>
        <td>"'.$row[0].'"</td>
        <td>"'.$row[1].'"</td>
        <td>"'.$row[2].'"</td>
        <td>"'.$row[3].'"</td>
        <td>"'.$row[4].'"</td>
        <td>"'.$row[5].'"</td>
        <td>"'.$row[6].'"</td>
        <td>non disponibile</td>
        <td><a href="home.php?source=9&IDp='.$row[7].'&IDe='.$row[8].'" class="btn btn-danger">Elimina</a>
        <a href="home.php?source=10&IDp='.$row[7].'&IDe='.$row[8].'" class="btn btn-primary">Commenta</a></td>
      </tr>
        ');
    }else{
      print('
        <tr>
          <td>"'.$row[0].'"</td>
          <td>"'.$row[1].'"</td>
          <td>"'.$row[2].'"</td>
          <td>"'.$row[3].'"</td>
          <td>"'.$row[4].'"</td>
          <td>"'.$row[5].'"</td>
          <td>"'.$row[6].'"</td>
          <td><a href="home.php?source=11&file='.mysqli_fetch_row($presentazione)[0].'" class="btn btn-success">Scarica</a></td>
          <td><a href="home.php?source=9&IDp='.$row[7].'&IDe='.$row[8].'" class="btn btn-danger">Elimina</a>
          <a href="home.php?source=10&IDp='.$row[7].'&IDe='.$row[8].'" class="btn btn-primary">Commenta</a></td>
        </tr>
          ');
    }
  }
  $costo = queryCostoUsr();
  if($costo!=false){
    print('<p>Con queste prenotazioni devi pagare: '.$costo.'€</p>');
	}
	}
}

function queryCostoUsr(){
  $db=mysqli_connect('127.0.0.1','root','') or die('Connessione al server fallita  ' . mysqli_error($db));
  mysqli_select_db($db, 'conference') or die('Impossibile accedere al db conference '. mysqli_error($db));
if($_SESSION['isAutore'] ==false){
  $query = "SELECT costo FROM persona WHERE IDpersona=".$_SESSION['userID'];
}

  $res = mysqli_query($db,$query);

  if(mysqli_num_rows($res)>0){
    return mysqli_fetch_row($res)[0];
  }
  return false;
}
  ?>
</div>
</div>
