<?php $action="home.php?source=7&event=".$_GET['event']; ?>
	<div class="container">
		<div class="card w-50">
			<div class="card-body">
				<span class="page_info">
					Inserisci il file di presentazione!
				</span>
				<form action=<?php echo $action;?> method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<input id="pres" name="presentazione" type="file" class="file">
					<div id="mod_usr_data_buttons" style="padding-left: 30%;">
						<button type="submit" class="btn btn-primary" style="margin-top: 10px;">Procedi</button>
						<a href="home.php?source=4" class="btn btn-danger" style="margin-top: 10px;">Torna indietro</a>
					</div>
				</div>
				</form>
			</div>

		<?php

		if(isset($_FILES["presentazione"])){
			$target_dir = "uploads/".$_SESSION['userID'].'/';
			if(!is_dir($target_dir)){
				$make = mkdir($target_dir);
				if(!$make)  echo "Impossibile creare la cartella relativa al tuo utente!";
			}
			$target_file = $target_dir . basename($_FILES["presentazione"]["name"]);
			print("<script>console.log('".$target_file."')</script>");
			if (move_uploaded_file($_FILES["presentazione"]["tmp_name"], $target_file)) {
				if(updateDB($target_file, basename( $_FILES["presentazione"]["name"]))!="error"){
					print('
						<div class="alert alert-success" role="alert">
							<strong>Successo!</strong> Il file '.basename( $_FILES["presentazione"]["name"]).' è stato caricato.
						</div>
					');
				}else{
				print('
					<div class="alert alert-danger" role="alert">
						<strong>Errore!</strong> Non è stato possibile aggiornare il database, il file sarà eliminato dal server
					</div>
					');
				}
			}else{
				print('
					<div class="alert alert-danger" role="alert">
						<strong>Errore!</strong> Info -> '.$_FILES['userfile']['error'].'
					</div>
				');
			}
		}

		function updateDB($path, $filename){

		$db=mysqli_connect('127.0.0.1','root','') or die('Connessione al server fallita  ' . mysqli_error($db));
	    mysqli_select_db($db, 'conference') or die('Impossibile accedere al db conference '. mysqli_error($db));
	    $filename = stripslashes($filename);
	    $filename = mysqli_real_escape_string($db,$filename);
	    $path = stripslashes($path);
	    $path = mysqli_real_escape_string($db,$path);
	    $evento = stripslashes($_GET['event']);
	    $evento = mysqli_real_escape_string($db,$evento);
	    //$queryUpdate = 'UPDATE presentazione SET filename = "'.$filename.'",path="'.$path.'" WHERE IDautore = "'.$_SESSION['userID'].'" AND IDevento="'.$evento.'"';
	    $queryUpdate = 'INSERT INTO `presentazione`( `filename`, `path`,`IDevento`,`IDautore`) VALUES ("'.$filename.'","'.$path.'","'.$evento.'","'.$_SESSION['userID'].'")';
	    $result=mysqli_query($db,$queryUpdate) or die('Query non eseguibile  '. mysqli_error($db));

		if(!$result){
			return('error');
		}else{
			return('ok');
		}
		}
		?>
	</div>
</div>
