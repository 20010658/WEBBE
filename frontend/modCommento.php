
<?php
if(isset($_GET['IDp']) && isset($_GET['IDe'])){
  $action="home.php?source=10&IDp=".$_GET['IDp']."&IDe=".$_GET['IDe'];
  $db=mysqli_connect('127.0.0.1','root','') or die('Connessione al server fallita  ' . mysqli_error());
  mysqli_select_db($db, 'conference') or die('Impossibile accedere al db conference '. mysqli_error());
  $IDp =  mysqli_real_escape_string($db,$_GET['IDp']);
  $IDp = basename($IDp);
  $IDe =  mysqli_real_escape_string($db,$_GET['IDe']);
  $IDe = basename($IDe);
  $query="SELECT commento FROM partecipa WHERE IDpersona=".$IDp." AND IDevento=".$IDe;
?>
<div class="container">
  <div class="card w-50">
    <div class="card-body">
      <form action=<?php echo($action) ?> method="POST">
        <span class="page_info">
          <?php
            $res = mysqli_query($db,$query);
            $c = mysqli_fetch_row($res)[0];
            if($c!=null){
              print('Hai già inserito un commento! <br>'.$c);
            }else{
              print("Inserisci il tuo commento all'evento!");
            }
           ?>
        </span>
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Commento" aria-describedby="basic-addon1" name="commento"/>
          <div id="mod_usr_data_buttons" style="padding-left: 30%;">
            <button type="submit" class="btn btn-primary" style="margin-top: 10px;">Procedi</button>
            <a href="home.php?source=4" class="btn btn-danger" style="margin-top: 10px;">Torna indietro</a>
          </div>
        </div>
        <?php
          if(isset($_POST['commento'])){
            $commento = mysqli_real_escape_string($db,$_POST['commento']);
            $commento = basename($commento);
			if($_SESSION['isAutore']==false){
				$query = 'UPDATE partecipa SET commento="'.$commento.'" WHERE IDpersona="'.$IDp.'" AND IDevento="'.$IDe.'"';
			}
			else{
				$query = 'UPDATE partecipa_autore SET commento="'.$commento.'" WHERE IDautore="'.$IDp.'" AND IDevento="'.$IDe.'"';
			}
            $res = mysqli_query($db,$query);
            if($res){
              print('
                <div class="alert alert-success" role="alert">
                  <strong>Successo!</strong> Il commento è stato salvato.
                </div>
                ');
            }else{
              print('
                <div class="alert alert-danger" role="alert">
                  <strong>Errore!</strong> Ci sono problemi di sistema, ti preghiamo di riprovare più tardi :(
                </div>
                ');
            }
          }
        }
         ?>
