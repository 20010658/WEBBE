<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Conference</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="styles.css" type="text/css">
</head>
<body>
	<div class="jumbotron text-center">
		<h1>Registrazione</h1>
		<p>Inserisci i tuoi dati!</p>
	</div>
	<div class="container">
		<div class="card w-50">
			<div class="card-body">
				<div class="alert alert-warning" role="alert">
					<strong>Success!</strong>Registrazione andata a buon fine, clicca sul link per continuare <a href="http://localhost/Web/registrazioneMail.php?conf=true">Link di Conferma</a>
				</div>
				<?php
					if(isset($_GET['conf'])){
						$click=stripslashes($_GET['conf']);
						if($click){
							print('
								<div class="alert alert-success" role="alert">
									<strong>Successo!</strong> Ok! Clicca per entrare nel sistema! -><a href="http://localhost/Web/home.php?source=1">Alpha Conference</a>
								</div>
							');
						}
					}
				?>
			</div>
		</div>
	</div>
</body>