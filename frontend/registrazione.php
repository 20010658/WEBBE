<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Conference</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
		<link rel="stylesheet" href="styles.css" type="text/css">
	</head>
	<body>
		<div class="jumbotron text-center">
			<h1>Registrazione Utente</h1>
			<p>
				Inserisci i tuoi dati!
			</p>
		</div>
		<div class="container">
			<form action="registrazione.php" method="POST">
				<div class="form-group">
					<label for="eMail">Nome</label>
					<input type="text" class="form-control" id="nome" name="nome">
				</div>
				<div class="form-group">
					<label for="eMail">Cognome</label>
					<input type="text" class="form-control" id="cognome" name="cognome">
				</div>
				<div class="form-group">
					<label for="eMail">Data di nascita</label>
					<input type="date" class="form-control" id="data_nascita" name="data_nascita" placeholder="anno/mese/giorno">
				</div>
				<div class="form-group">
					<label for="eMail">Afferenza</label>
					<input type="text" class="form-control" id="aff" name="afferenza">
				</div>
				<div class="form-group">
					<label for="eMail">Nazione</label>
					<input type="text" class="form-control" id="nazione" name="nazione">
				</div>
				<div class="form-group">
					<label for="eMail">Email</label>
					<input type="email" class="form-control" id="eMail" name="eMail" aria-describedby="emailHelp" placeholder="Enter email">
					<small id="emailHelp" class="form-text text-muted">Non condivideremo mai la tua mail con nessuno.</small>
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" class="form-control" id="password" name="password" placeholder="Password">
				</div>
				<div class="form-group">
					<label for="password">Reinserisci Password</label>
					<input type="password" class="form-control" id="passwordR" name="passwordR" placeholder="Password">
				</div>
				<button type="submit" class="btn btn-primary">Procedi!</button>
			</form>
		</div>
		
		<?php
		
			
			if(isset($_POST['nome']) && isset($_POST['cognome']) && isset($_POST['data_nascita']) && isset($_POST['afferenza']) && isset($_POST['nazione']) && isset($_POST['eMail']) && isset($_POST['password']) && isset($_POST['passwordR'])){
				$db=mysqli_connect('127.0.0.1','root','') or die('Connessione al server fallita  ' . mysqli_error());
				mysqli_select_db($db, 'conference') or die('Impossibile accedere al db  '. mysqli_error());
				$password = stripslashes($_POST['password']);
				$passwordR = stripslashes($_POST['passwordR']);
				
				if($password==$passwordR){
					$password = mysqli_real_escape_string($db,$password);
					$password = hash('sha256', $password);
						
					$query="INSERT INTO `persona` (`IDpersona`, `nome`, `cognome`, `data_nascita`, `afferenza`, `nazione`, `costo`, `eMail`, `password`) VALUES (NULL, '".$_POST['nome']."', '".$_POST['cognome']."', '".$_POST['data_nascita']."', '".$_POST['afferenza']."', '".$_POST['nazione']."', '', '".$_POST['eMail']."', '".$password."')";
					$result = mysqli_query($db,$query);
					if(!$result){
					print('
					<div class="alert alert-danger" role="alert">
						<strong>Errore!</strong> Ci sono problemi di sistema, ti preghiamo di riprovare più tardi :(
					</div>
					');
					}
					else{
						header('Location: index.php');
					}
				
				}else{

					header('Location: registrazione.php');
				}
			}
		?>
	</body>
</html>
