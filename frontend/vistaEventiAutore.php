<?php
function queryEventi(){
      $db=mysqli_connect('127.0.0.1','root','') or die('Connessione al server fallita  ' . mysqli_error($db));
      mysqli_select_db($db, 'conference') or die('Impossibile accedere al db conference '. mysqli_error($db));
      $queryEventiConferenza="SELECT * FROM evento e,presentazione p,conferenza c, autore a WHERE  e.IDevento=p.IDevento AND c.IDconferenza=e.IDconferenza";


      $resEvConf=mysqli_query($db,$queryEventiConferenza) or die('Query non eseguibile  '. mysqli_error($db));

      if(mysqli_num_rows($resEvConf) == 0){
        return('error');
      }else{
        return($resEvConf);
      }
}
?>
<div class="container">
  <div class="row text-center">
    <span class="page_info">
      <h3>Ecco gli eventi di cui sei autore!</h3>
      <h6>Cliccando sul bottone "Aggiungi" puoi caricare il file della presentazione</h6>
    </span>
      <table class="table">
      <thead>
        <tr>
          <th>Conferenza</th>
          <th>Titolo evento</th>
          <th>Aula</th>
          <th>Ora</th>
          <th>Giorno</th>
          <th>File Presentazione</th>
          <th>Azione</th>
        </tr>
      </thead>
      <tbody>
          <?php
            $result = queryEventi();
            if($result=='error'){
              print("<h1>Nessun evento disponibile nel Database! :(</h1>");
            }else{
              while($row = mysqli_fetch_row($result)){
                if($row[11]!=null){
                    print('
                    <tr>
                      <td>"'.$row[16].'"</td>
                      <td>"'.$row[1].'"</td>
                      <td>"'.$row[4].'"</td>
                      <td>"'.$row[5].'"</td>
                      <td>"'.$row[7].'"</td>
                      <td>"'.$row[11].'"</td>
                      <td><a href="home.php?source=8&event='.$row[0].'" class="btn btn-success">Modifica</a></td>
                    </tr>
                    ');
                }else{
                  print('
                  <tr>
                    <td>"'.$row[16].'"</td>
                    <td>"'.$row[1].'"</td>
                    <td>"'.$row[4].'"</td>
                    <td>"'.$row[5].'"</td>
                    <td>"'.$row[7].'"</td>
                    <td>non presente</td>
                    <td><a href="home.php?source=7&event='.$row[0].'" class="btn btn-success">Aggiungi</a></td>
                  </tr>
                  ');
                }
              }
            }
           ?>
      </tbody>
    </table>
  </div>
</div>
