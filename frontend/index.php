<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF16_bin" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="styles.css" type="text/css">
	<title>Conference</title>
</head>
<body>
	<div class="jumbotron text-center">
		<h1>Login</h1>
		<p>
		Autenticati per poter accedere al sito, oppure : <a href="registrazione.php">Registrati</a>
		</p>
	</div>
	<div class="container">
		<form action="index.php" method="POST">
			<div class="form-group">
			<label for="eMail">Email</label>
			<input type="email" class="form-control" id="eMail" name="eMail" aria-describedby="emailHelp" placeholder="Enter email">
			<small id="emailHelp" class="form-text text-muted">Non condivideremo la tua mail con nessuno.</small>
			</div>
			<div class="form-group">
			<label for="password">Password</label>
			<input type="password" class="form-control" id="password" name="password" placeholder="Password">
			</div>
			<button type="submit" class="btn btn-primary">Procedi!</button>
		</form>
	</div>
	<?php
	if(isset($_SESSION)){
		session_start();
	}
	
	if(!isset($_SESSION['userID'])){
		if(isset($_POST['eMail']) && isset($_POST['password'])){
				$db=mysqli_connect('127.0.0.1','root','') or die ('Connessione falita al server'.mysqli_error($db));
				mysqli_select_db($db,'conference') or die ('Connessione al database fallita' .mysqli_error($db));
				
				$eMail = stripslashes($_POST['eMail']);
				$eMail = mysqli_real_escape_string($db,$eMail);
				$password = stripslashes($_POST['password']);
				$password = mysqli_real_escape_string($db,$password);
				$password = hash('sha256', $password);
				$query="SELECT IDpersona,nome,cognome FROM persona WHERE eMail='".$eMail."' AND password='".$password."'";
				
				$result=mysqli_query($db,$query) or die('Query non eseguibile  '. mysqli_error());
			
				if ($result!=null && mysqli_num_rows($result)==0) {
					//controllo se l'utente è un autore
					$query="SELECT IDautore,nome,cognome FROM autore WHERE  eMail='".$eMail."'";
					$result=mysqli_query($db,$query) or die('Query non eseguibile  '. mysqli_error());
					if($result==null &&  mysqli_num_rows($result)==0){
						print('<span class="page_info"><h1> Accesso Negato </h1></span>');
					}
					else{
						session_start();
						$row = mysqli_fetch_row($result);
						$_SESSION['userID'] = $row[0];
						$_SESSION['userNOME'] =$row[1];
						$_SESSION['userCOGNOME'] = $row[2];
						$_SESSION['isAutore'] = true;
						//setto il time to live, 1500 secondi
						$_SESSION['timeToLive'] = time() + 1500;
						header('Location: home.php?source=16');
					}
				}
				else{
					session_start();
					$row = mysqli_fetch_row($result);
					$_SESSION['userID'] = $row[0];
					$_SESSION['userNOME'] =$row[1];
					$_SESSION['userCOGNOME'] = $row[2];
					$_SESSION['isAutore'] = false;
					//setto il time to live, 1500 secondi
					$_SESSION['timeToLive'] = time() + 1500;
					header('Location: home.php?source=16');
				}
		
		}
	
	}
	else{
		header('Location: home.php?source=0');
	}
	?>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
	</body>
</html>