<div class="container">
  <div class="row text-center">
<?php
if(isset($_GET['IDp']) && isset($_GET['IDe'])){
  $db=mysqli_connect('127.0.0.1','root','') or die('Connessione al server fallita  ' . mysqli_error($db));
  mysqli_select_db($db, 'conference') or die('Impossibile accedere al db conference '. mysqli_error($db));

  $idp = stripslashes($_GET['IDp']);
  $idp = mysqli_real_escape_string($db,$idp);
  $ide = stripslashes($_GET['IDe']);
  $ide = mysqli_real_escape_string($db,$ide);
	if($_SESSION['isAutore'] ==false){
		$query = "DELETE FROM partecipa WHERE IDpersona=".$idp." AND IDevento=".$ide;
	}
	else{
		$query= "DELETE FROM partecipa_autore WHERE IDautore=".$idp." AND IDevento=".$ide;
	}
  $result = mysqli_query($db,$query) or die('Query non eseguibile  '. mysqli_error($db));
  if(!$result){
    print('<span class="page_info"><h2>Errore, non è stato possibile cancellare la prenotazione, riprovare più tardi</h2></span>');
  }else{
    print('<span class="page_info"><h2>La prenotazione è stata cancellata con successo! :)</h2></span>');
  }
}
 ?>
  </div>
</div>
