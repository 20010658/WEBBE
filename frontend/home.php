<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF16_bin" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="styles.css" type="text/css">
	<title>Conference</title>
</head>
<body>
	<?php
		if(!isset($_SESSION)){
			session_start();
		}
		
		$now = time();
		if($_SESSION['timeToLive']<$now){
			session_unset();
			session_destroy();
			// In caso la sessione sia scaduta redirigo l'output al login
			header('Location:index.php');
		}
		
		$_SESSION['timeToLive'] = $now+1500;
		include ('header.php');
		if(isset($_GET['source'])){
			$source=stripcslashes($_GET['source']);
			$source= basename($source);			
		
		switch($source){

				case 0:
					if($_SESSION['isAutore']==true)
						$action="conferenze_autore.php";
					else
					$action='conferenze.php';
					break;
				case 1:
					$action='eventi.php';
					break;
				case 2:
					$action='handleSelezioneEventi.php';
					break;
				case 3:
					$action="usrDatas.php";
					break;
				case 4:
					$action='usrPrenotazioni.php';
					break;
				case 5:
					$action='modUsrData.php';
					break;
				case 6:
					$action='vistaEventiAutore.php';
					break;
				case 7:
					$action='addPresentazione.php';
					break;
				case 8:
					$action='modPresentazione.php';
					break;
				case 9:
					$action='deletePrenotazione.php';
					break;
				case 10:
					$action='modCommento.php';
					break;
				case 11:
					$action='download.php';
					break;
				case 12:
					$action='commenti.php';
					break;
				case 13:
					$action='eventi_autore.php';
					break;
				case 14:
					$action='commenti_privati.php';
					break;
				case 15:
					$action='handleSelezioneEventiAUT.php';
					break;
				case 16:
					$action='start.php';
					break;
				case 17:
					$action='conferenze_autore.php';
					break;
				default:
					$action="error404.html";
					break;
			}
			include($action);
		}
	?>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</body>
</html>