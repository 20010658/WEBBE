

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.Connection;

/**
 * Servlet implementation class Iscritti
 */
@WebServlet("/Iscritti")
public class Iscritti extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Iscritti() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		DBconnection con = new DBconnection();
		Connection c;
		PreparedStatement res;
		PrintWriter w = response.getWriter();
		try {
			c = (Connection) con.get_access();
			res = c.prepareStatement("SELECT * FROM persona");
			ResultSet r=res.executeQuery();
			w.println("<html>");
			w.println("<head> <title>Administration</title>"
					+ "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css\" integrity=\"sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M\" crossorigin=\"anonymous\">"
					+ "</head>");
			w.println("<body>");
			w.println("<div class=\"jumbotron text-center\">\r\n" + 
					"  <h1>Conference Administration</h1>\r\n" + 
					"  <p>Sistema di prenotazione on-line</p>\r\n" + 
					"<p><a href=\"http://localhost:8080/Administration/iscritti\">Visualizza tutti gli iscritti</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/ricerca_iscritti\">Ricerca iscritti</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/mod_conf\">Modifica Conferenze</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/mod_ev\">Modifica Evento</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/create_autore\">Crea Autore</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/autore_presentazione\">Assegna Autore a Presentazione</a></p>"+
					"  </div>");
			w.println("<div class=\"container\">\r\n" + 
					"    <div class=\"row\">\r\n" + 
					"        <div class=\"col-md-12\">\r\n" + 
					"            <div class=\"panel-body\">\r\n");
			w.println("Ecco tutti gli iscritti \r\n"+
					"     <table class=\"table table-hover table-striped table-bordered\">\r\n"+
					"      <thead>\r\n" + 
					"        <tr>\r\n" + 
					"          <th>Nome</th>\r\n" + 
					"          <th>Cognome</th>\r\n" + 
					"          <th>Data di nascita</th>\r\n" + 
					"          <th>Afferenza</th>\r\n" + 
					"          <th>Nazione</th>\r\n" + 
					"          <th>Costo</th>\r\n" + 
					"          <th>email</th>\r\n" +
					"			<th>azione</th>\r\n" +
					"        </tr>\r\n" + 
					"      </thead>\r\n" + 
					"      <tbody>");
			while(r.next()) {
				w.println("<tr><td>"+r.getString(2)+"</td>");
				w.println("<td>"+r.getString(3)+"</td>");
				w.println("<td>"+r.getString(4)+"</td>");
				w.println("<td>"+r.getString(5)+"</td>");
				w.println("<td>"+r.getString(6)+"</td>");
				w.println("<td>"+r.getString(7)+"</td>");
				w.println("<td>"+r.getString(8)+"</td></tr>"); 
				w.println("<td>"+r.getString(9)+"</td></tr>"); 
			}
			
			while(r.next()) {
				w.println("<tr><form action=\"http://localhost:8080/Administration/mod_conf\" method=\"POST\">"
						+"<td>"+r.getString(2)+"</td>");
				w.println("<td>"+r.getString(3)+"</td>");
				w.println("<td>"+r.getString(4)+"</td>");
				w.println("<td>"+r.getString(5)+"</td>");
				w.println("<td>"+r.getString(6)+"</td>");
				w.println("<td>"+r.getString(7)+"</td>");
				w.println("<td>"+r.getString(8)+"</td>");
				w.println("<td>"+r.getString(9)+"</td></tr>"); 
				w.println("<td>"
						+ "<input type=\"hidden\" name=\"persona\" value=\""+r.getString(1)+"\">"
								+ "<input type=\"submit\" name=\"b\" value=\"Cancella\" class=\"btn btn-danger\"></td></form></tr>");
			}
			w.println("<tr><form action=\"http://localhost:8080/Administration/Iscritti\" method=\"POST\">"
					+"<td><input type=\"text\" name=\"IDpersona\"</td>");
			w.println("<td><input type=\"text\" name=\"nome\"</td>");
			w.println("<td><input type=\"text\" name=\"cognome\"</td>");
			w.println("<td><input type=\"text\" name=\"data_nascita\"</td>");
			w.println("<td><input type=\"text\" name=\"afferenza\"</td>");
			w.println("<td><input type=\"text\" name=\"nazione\"</td>");
			w.println("<td><input type=\"text\" name=\"costo\"</td>");
			w.println("<td><input type=\"text\" name=\"email\"</td>");
			w.println("<td><input type=\"submit\" name=\"b\" value=\"Aggiungi\" class=\"btn btn-success\"></td></form></tr>");
			
			w.println("</tbody>\r\n" + 
					"    </table>");
			w.println("  </tbody>\r\n" + 
					"    </table>");
			w.println("</div>"
					+ "</div>"
					+ "</div>"
					+ "</div>");
			w.println("  <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>\r\n" + 
					"  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js\" integrity=\"sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4\" crossorigin=\"anonymous\"></script>\r\n" + 
					"  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js\" integrity=\"sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1\" crossorigin=\"anonymous\"></script>\r\n" 
					);
			w.println("</body>");
			w.println("</html>");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
