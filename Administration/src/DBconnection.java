import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.UnavailableException;
public class DBconnection {
public Connection connection;
	
	public DBconnection(){
		
	}
	public Connection get_access() throws SQLException{
		try {
			Class.forName("com.mysql.jdbc.Driver" );
			connection= DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/conference", "root", "");
			System.out.println("connesso");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connection;
	}
	
	public void destroy(){
		try{
			connection.close();
		}
		catch(SQLException e){
			
		e.printStackTrace();
		}
	}
}
