

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

/**
 * Servlet implementation class CreateAutore
 */
@WebServlet("/CreateAutore")
public class CreateAutore extends HttpServlet {
	DBconnection con;
	Connection c;
	PreparedStatement res;
	ResultSet r;
	PrintWriter w;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateAutore() {
        super();
        con = new DBconnection();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		w = response.getWriter();
		try {
			c = (Connection) con.get_access();
			res = c.prepareStatement("SELECT * FROM autore");
			r=res.executeQuery();
			w.println("<html>");
			w.println("<head> <title>Administration</title>"
					+ "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css\" integrity=\"sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M\" crossorigin=\"anonymous\">"
					+ "</head>");
			w.println("<body>");
			w.println("<div class=\"jumbotron text-center\">\r\n" + 
					"  <h1>Alpha Conference Administration</h1>\r\n" + 
					"  <p>Sistema di prenotazione on-line</p>\r\n" + 
					"<p><a href=\"http://localhost:8080/Administration/iscritti\">Visualizza tutti gli iscritti</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/ricerca_iscritti\">Ricerca iscritti</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/mod_conf\">Modifica Conferenze</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/mod_ev\">Modifica Evento</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/create_autore\">Crea Autore</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/autore_presentazione\">Assegna Autore a Presentazione</a></p>"+
					"  </div>");
			w.println("<div class=\"container\">\r\n" + 
					"    <div class=\"row\">\r\n" + 
					"        <div class=\"col-md-12\">\r\n" + 
					"            <div class=\"panel-body\">\r\n");
			w.println("Ecco tutti gli autori, puoi aggiungere o cancellare (in caso di aggiunta saranno notificati alla mail fornita con la password temporanea che stabilisci) \r\n"+
					"     <table class=\"table table-hover table-striped table-bordered\" style=\"table-layout: fixed;\">\r\n"+
					"      <thead>\r\n" + 
					"        <tr>\r\n" + 
					"          <th>Nome</th>\r\n" + 
					"          <th>Cognome</th>\r\n" + 
					"          <th>Data di nascita</th>\r\n" + 
					"          <th>Affiliazione</th>\r\n" + 
					"          <th>eMail</th>\r\n" + 
					"          <th>Password</th>\r\n" + 
					"          <th>Azione</th>\r\n" +
					"        </tr>\r\n" + 
					"      </thead>\r\n" + 
					"      <tbody>");
			while(r.next()) {
				w.println("<tr><form action=\"http://localhost:8080/Administration/create_autore\" method=\"POST\">"
						+"<td>"+r.getString(2)+"</td>");
				w.println("<td>"+r.getString(3)+"</td>");
				w.println("<td>"+r.getString(4)+"</td>");
				w.println("<td>"+r.getString(5)+"</td>");
				w.println("<td>"+r.getString(6)+"</td>");
				if(r.getString(7).equals("a6864eb339b0e1f6e00d75293a8840abf069a2c0fe82e6e53af6ac099793c1d5")) {
					w.println("<td>password temporanea</td>");
				}else {
					w.println("<td>password definita da usr</td>");
				}
				w.println("<td>"
						+ "<input type=\"hidden\" name=\"autID\" value=\""+r.getString(1)+"\">"
								+ "<input type=\"submit\" name=\"b\" value=\"Cancella\" class=\"btn btn-danger\"></td></form></tr>");
			}
			w.println("<tr><form action=\"http://localhost:8080/Administration/create_autore\" method=\"POST\">"
					+"<td><input type=\"text\" name=\"nome\"</td>");
			w.println("<td><input type=\"text\" name=\"cognome\"</td>");
			w.println("<td><input type=\"text\" name=\"data_nascita\"</td>");
			w.println("<td><input type=\"text\" name=\"afferenza\"</td>");
			w.println("<td><input type=\"text\" name=\"eMail\"</td>");
			w.println("<td><input type=\"text\" name=\"password\"</td>");
			w.println("<td><input type=\"submit\" name=\"b\" value=\"Aggiungi\" class=\"btn btn-success\"></td></form></tr>");
			
			w.println("</tbody>\r\n" + 
					"    </table>");
			
			if(request.getParameterMap().containsKey("del") && request.getParameter("del").equals("true")) {
				w.println("<p>Cancellazione effettuata con successo</p>");
			}else if(request.getParameterMap().containsKey("del") && request.getParameter("del").equals("false")) {
				w.println("<p>Errore nella cancellazione</p>");
			}
			if(request.getParameterMap().containsKey("add") && request.getParameter("add").equals("true")) {
				w.println("<p>Aggiunta effettuata con successo</p>");
			}else if(request.getParameterMap().containsKey("add") && request.getParameter("add").equals("false")) {
				w.println("<p>Errore nell'aggiunta</p>");
			}
			w.println("</div>"
					+ "</div>"
					+ "</div>"
					+ "</div>");
			w.println("  <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>\r\n" + 
					"  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js\" integrity=\"sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4\" crossorigin=\"anonymous\"></script>\r\n" + 
					"  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js\" integrity=\"sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1\" crossorigin=\"anonymous\"></script>\r\n" 
					);
			w.println("</body>");
			w.println("</html>");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String whichButton=request.getParameter("b");
		Statement s;
		String query;
		w = response.getWriter();
		try {
			c = (Connection) con.get_access();
		
		switch(whichButton) {
		case "Aggiungi":
			try {
				MessageDigest digest = MessageDigest.getInstance("SHA-256");
				byte[] encodedhash = digest.digest(
				  request.getParameter("password").getBytes(StandardCharsets.UTF_8));
			String password =  encodedhash.toString();
			query="INSERT INTO autore VALUES(NULL,\""+request.getParameter("nome")+"\",\""+request.getParameter("cognome")+"\",\""+request.getParameter("data_nascita")+"\",\""+request.getParameter("afferenza")+"\",\""+request.getParameter("eMail")+"\",\""+password+"\")";
			res = c.prepareStatement(query);
			res.execute();
			response.sendRedirect("http://127.0.0.1:8080/Administration/create_autore?add=true");
			} catch (SQLException e) {
				e.printStackTrace();
				
				response.sendRedirect("http://127.0.0.1:8080/Administration/create_autore?add=false");
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			break;
		case "Cancella":
			try {
				System.out.println("confID="+request.getParameter("confID"));
				query = "DELETE FROM autore WHERE IDautore=\""+request.getParameter("autID")+"\"";
				res = c.prepareStatement(query);
				res.execute();
				response.sendRedirect("http://127.0.0.1:8080/Administration/create_autore?del=true");
			} catch (SQLException e) {
				e.printStackTrace();
				response.sendRedirect("http://127.0.0.1:8080/Administration/create_autore?del=false");
			}
			break;
		}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
