

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.Connection;

/**
 * Servlet implementation class AutorePresentazione
 */
@WebServlet("/AutorePresentazione")
public class AutorePresentazione extends HttpServlet {
	DBconnection con;
	Connection c;
	PreparedStatement res;
	ResultSet r;
	PrintWriter w;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AutorePresentazione() {
        super();
        con = new DBconnection();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		w = response.getWriter();
		try {
			c = (Connection) con.get_access();
			res = c.prepareStatement("SELECT IDautore,nome,cognome FROM autore");
			r=res.executeQuery();
			w.println("<html>");
			w.println("<head> <title>Administration</title>"
					+ "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css\" integrity=\"sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M\" crossorigin=\"anonymous\">"
					+ "</head>");
			w.println("<body>");
			w.println("<div class=\"jumbotron text-center\">\r\n" + 
					"  <h1>Alpha Conference Administration</h1>\r\n" + 
					"  <p>Sistema di prenotazione on-line</p>\r\n" + 
					"<p><a href=\"http://localhost:8080/Administration/iscritti\">Visualizza tutti gli iscritti</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/ricerca_iscritti\">Ricerca iscritti</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/mod_conf\">Modifica Conferenze</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/mod_ev\">Modifica Evento</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/create_autore\">Crea Autore</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/autore_presentazione\">Assegna Autore a Presentazione</a></p>"+
					"  </div>");
			
		
			w.println("<div class=\"container\">\r\n" + 
					"    <div class=\"row\">\r\n" + 
					"        <div class=\"col-md-12\">\r\n" + 
					"            <div class=\"panel-body\">\r\n"
					+ "				<form action=\"http://localhost:8080/Administration/autore_presentazione\" method=\"POST\">");
			w.println("					<p>Seleziona l'autore: <select name=\"aut\">");
			res = c.prepareStatement("SELECT IDautore,nome,cognome FROM autore");
			r=res.executeQuery();
			while(r.next()) {
				w.println("				<option value=\""+r.getString(1)+"\">"+r.getString(2)+" "+r.getString(3)+"</option>");		
			}
			w.println("</select></p>");
			w.println("					<p>Seleziona l'evento: <select name=\"ev\">");
			res = c.prepareStatement("SELECT * FROM evento");
			r=res.executeQuery();
			while(r.next()) {
				w.println("				<option value=\""+r.getString(1)+"\">"+r.getString(2)+"</option>");		
			}
			w.println("</select></p>");
			w.println("<input type=\"submit\" name=\"b\" value=\"Crea Associazione\" class=\"btn btn-success\">"
					+ "</form>");
			if(request.getParameterMap().containsKey("add") && request.getParameter("add").equals("true")) {
				w.println("<p>Associazione creata con successo</p>");
				System.out.println(request.getParameterMap().containsKey("add")+"-----"+request.getParameter("add"));
			}else if(request.getParameterMap().containsKey("add") && request.getParameter("add").equals("false")) {
				w.println("<p>Errore nella creazione dell'associazione, riprova</p>");
			}
			w.println("</div>"
					+ "</div>"
					+ "</div>"
					+ "</div>");
			w.println("  <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>\r\n" + 
					"  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js\" integrity=\"sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4\" crossorigin=\"anonymous\"></script>\r\n" + 
					"  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js\" integrity=\"sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1\" crossorigin=\"anonymous\"></script>\r\n" 
					);
			w.println("</body>");
			w.println("</html>");
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			c = (Connection) con.get_access();
			es = c.prepareStatement("INSERT INTO presentazione VALUES(NULL,\""+request.getParameter("ev")+"\",\""+request.getParameter("aut")+"\")");
			res.execute();
			response.sendRedirect("http://127.0.0.1:8080/Administration/autore_presentazione?add=true");
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			response.sendRedirect("http://127.0.0.1:8080/Administration/autore_presentazione?add=false");
			e.printStackTrace();
		}
	}
}
