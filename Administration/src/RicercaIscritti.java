

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.Connection;

/**
 * Servlet implementation class RicercaIscritti
 */
@WebServlet("/RicercaIscritti")
public class RicercaIscritti extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RicercaIscritti() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter w = response.getWriter();
			w.println("<html>");
			w.println("<head> <title>Administration</title>"
					+ "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css\" integrity=\"sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M\" crossorigin=\"anonymous\">"
					+ "</head>");			w.println("<body>");
			w.println("<div class=\"jumbotron text-center\">\r\n" + 
					"  <h1>Alpha Conference Administration</h1>\r\n" + 
					"  <p>Sistema di prenotazione on-line</p>\r\n" + 
					"<p><a href=\"http://localhost:8080/Administration/iscritti\">Visualizza tutti gli iscritti</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/ricerca_iscritti\">Ricerca iscritti</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/mod_conf\">Modifica Conferenze</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/mod_ev\">Modifica Evento</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/create_autore\">Crea Autore</a></p>"+
					"<p><a href=\"http://localhost:8080/Administration/autore_presentazione\">Assegna Autore a Presentazione</a></p>"+
					"  </div>");
			w.println("<div class=\"container\">\r\n" + 
					"    <div class=\"row\">\r\n" + 
					"        <div class=\"col-md-12\">\r\n" + 
					"            <div class=\"panel-body\">\r\n");
			w.println("<p>Ricerca Iscritti</p>\r\n"+
					"<form action=\"http://localhost:8080/Administration/ricerca_iscritti\" method=\"POST\">");
			w.println("<p>Puoi cercare per nome e cognome, oppure per affiliazione</p>");
			w.println("<p>Nome:<input type=\"text\" name=\"nome\">");
			w.println("Cognome:<input type=\"text\" name=\"cognome\">");
			w.println("<input type=\"submit\" name=\"r\" value=\"Ricerca per Nome e Cognome\" class=\"btn btn-primary\"></p>\r\n");
			w.println("<p>Affiliazione:<input type=\"text\" name=\"affiliazione\">");
			w.println("<input type=\"submit\" name=\"r\" value=\"Ricerca per Affiliazione\" class=\"btn btn-primary\"></p>");
			w.println("</form>");
			w.println("</div>"
					+ "</div>"
					+ "</div>"
					+ "</div>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String whichButton = request.getParameter("r");
		DBconnection con = new DBconnection();
		ResultSet r;
		PrintWriter w;
		Connection c;
		PreparedStatement res;
		w = response.getWriter();
		w.println("<html>");
		w.println("<head> <title>Administration</title>"
				+ "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css\" integrity=\"sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M\" crossorigin=\"anonymous\">"
				+ "</head>");			w.println("<body>");
		w.println("<div class=\"jumbotron text-center\">\r\n" + 
				"  <h1>Alpha Conference Administration</h1>\r\n" + 
				"  <p>Sistema di prenotazione on-line</p>\r\n" + 
				"<p><a href=\"http://localhost:8080/Administration/iscritti\">Visualizza tutti gli iscritti</a></p>"+
				"<p><a href=\"http://localhost:8080/Administration/ricerca_iscritti\">Ricerca iscritti</a></p>"+
				"<p><a href=\"http://localhost:8080/Administration/mod_conf\">Modifica Conferenze</a></p>"+
				"<p><a href=\"http://localhost:8080/Administration/mod_ev\">Modifica Evento</a></p>"+
				"<p><a href=\"http://localhost:8080/Administration/create_autore\">Crea Autore</a></p>"+
				"<p><a href=\"http://localhost:8080/Administration/autore_presentazione\">Assegna Autore a Presentazione</a></p>"+
				"  </div>");
		w.println("<div class=\"container\">\r\n" + 
				"    <div class=\"row\">\r\n" + 
				"        <div class=\"col-md-12\">\r\n" + 
				"            <div class=\"panel-body\">\r\n");
		try {
			c = (Connection) con.get_access();
			switch(whichButton) {
			case "Ricerca per Nome e Cognome":
				res = c.prepareStatement("SELECT * FROM persona WHERE nome=\""+request.getParameter("nome")+"\" AND cognome=\""+request.getParameter("cognome")+"\"");
				r = res.executeQuery();
				w.println("Ecco il risultato della ricerca per nome e cognome \r\n"+
						"     <table class=\"table table-hover table-striped table-bordered\">\r\n"+
						"      <thead>\r\n" + 
						"        <tr>\r\n" + 
						"          <th>Nome</th>\r\n" + 
						"          <th>Cognome</th>\r\n" + 
						"          <th>Data di nascita</th>\r\n" + 
						"          <th>Afferenza</th>\r\n" + 
						"          <th>Nazione</th>\r\n" + 
						"          <th>Costo</th>\r\n" + 
						"          <th>email</th>\r\n" +
						"        </tr>\r\n" + 
						"      </thead>\r\n" + 
						"      <tbody>");
				while(r.next()) {
					w.println("<tr><td>"+r.getString(2)+"</td>");
					w.println("<td>"+r.getString(3)+"</td>");
					w.println("<td>"+r.getString(4)+"</td>");
					w.println("<td>"+r.getString(5)+"</td>");
					w.println("<td>"+r.getString(6)+"</td>");
					w.println("<td>"+r.getString(7)+"</td>");
					w.println("<td>"+r.getString(8)+"</td></tr>"); 
				}
				w.println("  </tbody>\r\n" + 
						"    </table>");
				break;
			case "Ricerca per Affiliazione":
				res = c.prepareStatement("SELECT * FROM persona WHERE afferenza=\""+request.getParameter("affiliazione")+"\"");
				r = res.executeQuery();
				w.println("Ecco il risultato della ricerca per affiliazione \r\n"+
						"     <table class=\"table table-hover table-striped table-bordered\">\r\n"+
						"      <thead>\r\n" + 
						"        <tr>\r\n" + 
						"          <th>Nome</th>\r\n" + 
						"          <th>Cognome</th>\r\n" + 
						"          <th>Data di nascita</th>\r\n" + 
						"          <th>Afferenza</th>\r\n" + 
						"          <th>Nazione</th>\r\n" + 
						"          <th>Costo</th>\r\n" + 
						"          <th>email</th>\r\n" +
						"        </tr>\r\n" + 
						"      </thead>\r\n" + 
						"      <tbody>");
				while(r.next()) {
					w.println("<tr><td>"+r.getString(2)+"</td>");
					w.println("<td>"+r.getString(3)+"</td>");
					w.println("<td>"+r.getString(4)+"</td>");
					w.println("<td>"+r.getString(5)+"</td>");
					w.println("<td>"+r.getString(6)+"</td>");
					w.println("<td>"+r.getString(7)+"</td>");
					w.println("<td>"+r.getString(8)+"</td></tr>"); 
				}
				w.println("  </tbody>\r\n" + 
						"    </table>");
				break;
			}
			w.println("</div>"
					+ "</div>"
					+ "</div>"
					+ "</div>");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
